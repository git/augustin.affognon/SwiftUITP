//
//  Colors.swift
//  MyFirstProject
//
//  Created by etudiant on 09/05/2023.
//

import Foundation
import SwiftUI
public struct WtaColor{
    
    static let cololor = Color("Color")
    static let dark = Color("dark")
    static let black_1 = Color("black_1")
    static let black_2 = Color("black_2")
    static let white = Color("white")
    static let font_icon = Color("font_icon")
    
}
