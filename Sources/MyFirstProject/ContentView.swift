//
//  ContentView.swift
//  MyFirstProject
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI
import Combine
struct ContentView: View {
    var album : Album?
    @State private var isExpanded = false

    var myContenView : CardImage?
   @State var isclicked : Bool = true
    @State public var color: Color = .white
    @Environment (\.presentationMode ) var presentationMode
    init(album: Album, myContenView: CardImage) {
        self.album = album
        self.myContenView = myContenView
    }
    private func createGridColumns() -> [GridItem] {
           return Array(repeating: GridItem(.flexible(), spacing: 10), count: 2)
       }
    var body: some View {
        
        NavigationView{
             
            ZStack(alignment: .top){
                ScrollView{
                     
                  
                   VStack {
                       
                       myContenView.onReceive(Just(UIImage(named: album?.image ?? "Background"))) { uiImage in
                           if let averageColor = uiImage?.averageColor {
                               self.color = Color(averageColor)
                           }
                       }.padding()
                       
                       Text(album?.nom ?? "Nom")
                           .font(.title2)
                           .bold()
                       HStack{
                           Image("Image")
                               .resizable()
                               .scaledToFill()
                               .frame(width: 25, height: 30)
                               .clipShape(Circle())
                               .overlay(
                                   Circle().stroke(Color.white, lineWidth: 2)
                               )
                               .shadow(radius: 5)
                           
                           Text(album?.type ?? "Type")
                               .font(.title3)
                               .bold()
                           
                           Image(systemName: "chevron.right")
                       }
                       
                       Button(action: {
                           self.isclicked = !isclicked
                       }) {
                           HStack{
                               Image(systemName: isclicked ? "play.fill" : "pause.fill");  Text("Dernière épisode")
                               
                           }.padding(.all )
                               .background(WtaColor.white).foregroundColor(WtaColor.black_1)
                               .cornerRadius(10)
                           
                       }
                       Grid(alignment : .leading){
                           GridRow{
                               Text(album?.description ?? "...").lineLimit(isExpanded ? nil : 2)
                                   .padding(.trailing, isExpanded ? 0 : 3).overlay(
                                    
                                    Button(action: {
                                        isExpanded.toggle()
                                    }) {
                                        Text( !isExpanded ?  "PLUS" : "MOINS").shadow(radius: 50 ).background(color)
                                            .foregroundColor(.indigo).bold().padding(.trailing)
                                    }.frame(alignment: .trailing).position(x:350,y:40)
                                    
                                   )
                                        
                              
                               
                           }.padding()
                          
                           GridRow( ){
                               
                               HStack(){
                                       Image(systemName: "star.fill")
                                       Text("4,4")
                                       Text("(1,8 k)")
                                       Text(". science")
                               } .padding(.leading)
                                
                           }
                       
                       }
                      
                       ListEpisodes()
                       Divider()
                      
                            
                   }.background(color)
                   
                }.onAppear()
               
                }.foregroundColor(WtaColor.cololor)
           
           // toolnbar
                
       
                
                .navigationBarItems(
                    leading:
                        Button(action: {
                            presentationMode.wrappedValue.dismiss()
                        }) {
                            Grid(alignment: .top){
                                GridRow {
                                    Image(systemName: "chevron.left")
                                         .scaledToFill()
                                        .frame(width: 20, height: 10)
                                        .padding()
                                        .clipShape(Circle())
                                        .overlay(
                                            Circle().stroke(Color.gray) )
                                        .background(Color.gray)
                                                    .clipShape(Circle())
                                                    .shadow(radius: 10)
                                     
                                   }
                            }
                        },
                
                        
                    trailing:
                        Button(action: {
                            // Action du bouton de droite
                        }) {
                            Grid(alignment: .trailing,horizontalSpacing: -15){
                                GridRow {
                                   
                                    Image(systemName: "arrow.down")
                                        .scaledToFill()
                                        .frame(width: 20, height: 10)
                                        .padding()
                                        .clipShape(Circle())
                                        .overlay(
                                            Circle().stroke(Color.gray) )
                                                    .background(Color.gray)
                                                    .clipShape(Circle())
                                                    .shadow(radius: 10)
                                        
                                       
                                         
                                    Image(systemName: "pause.fill")
                                        .scaledToFill()
                                        .frame(width: 5, height: 1)
                                        .padding().overlay(Circle().stroke(color) )
                                                .background(Color.white)
                                                .clipShape(Circle())
                                                .shadow(radius: 5)
                                                .position(x: 10,y:40)
                                    
                                    
                                    Image(systemName: "ellipsis")
                                        .scaledToFill()
                                        .frame(width: 20, height: 10)
                                        .padding()
                                        .clipShape(Circle())
                                        .overlay(
                                            Circle().stroke(Color.gray) )
                                                    .background(Color.gray)
                                                    .clipShape(Circle())
                                                    .shadow(radius: 10)
                                    
                                }
                                    
                                   
                                
                            }
                        }).background(color)
                             .overlay(
                                 VStack{
                                     Spacer()
                                    
                                     LecteurView(album: album).background()
                                    
                                 }
                               
                             )
                    
       //fi,
        }
        .navigationBarHidden(true)

    }
     
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
  // pour le thème
        Group{
            ContentView(album: Album(nom: "Augustin", type: "Rapp", image: "Image", ranking: 12),myContenView:  CardImage(album: Stub().load()))
            
            ContentView(album: Album(nom: "Augustin", type: "Rapp", image: "Image", ranking: 12),myContenView:  CardImage(album: Stub().load())).environment(\.colorScheme, .dark)
        }
       
    }
}
