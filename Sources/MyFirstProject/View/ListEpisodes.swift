//
//  ListEpisodes.swift
//  MyFirstProject
//
//  Created by etudiant on 17/05/2023.
//

import SwiftUI

struct ListEpisodes: View {
  


    var body: some View {
        ZStack{
            
            Grid{
                
                GridRow(alignment: .top){
                    
                    HStack{
                        Button(action: {}){
                            Text("Episodes")
                            Image(systemName: "chevron.down").bold().foregroundColor(WtaColor.font_icon)
                        }.padding()
                        
                    }
                   Divider()
                    Button(action : {}){
                        Text("Tout voir").foregroundColor(WtaColor.font_icon)
                    }.padding()
                }
                
                List {
                    ForEach(Stub().loadAll(), id: \.nom) { element in
                        EpisodeItem()
                           }
                       }
                .listStyle(GroupedListStyle())
                 
            }.padding(.vertical).background()
               
          
        }
        .frame(minHeight: 800)
        .background(Color.white).foregroundColor(WtaColor.black_2).padding(.vertical)
    }
}

struct ListEpisodes_Previews: PreviewProvider {
    static var previews: some View {
        ListEpisodes()
    }
}
