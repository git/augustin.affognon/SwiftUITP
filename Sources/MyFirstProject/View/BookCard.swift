//
//  BookCard.swift
//  MyFirstProject
//
//  Created by etudiant on 16/05/2023.
//

import SwiftUI

struct BookCard: View {
    var album : Album?
    init(album: Album) {
        self.album = album
    }
    var body: some View {
        Grid(alignment : .leading){
            GridRow{
                Rectangle()  .background(.gray)
                           .overlay(
                            Image(album?.image ?? "Image")
                           )
                           .frame(width: 180,height: 180)
                           .clipShape(RoundedRectangle(cornerRadius: 5))
                  }
            GridRow(){
                VStack(alignment:.leading){
                    Text(album?.nom ?? "").font(.subheadline).bold()
                        
                    Text("Mise à jour Mardi").font(.caption)
                }
            }
        }.foregroundColor(WtaColor.black_2)
       
    }
}

struct BookCard_Previews: PreviewProvider {
    static var previews: some View {
        BookCard(album: Stub().load())
    }
}
