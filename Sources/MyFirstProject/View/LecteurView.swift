//
//  LecteurViw.swift
//  MyFirstProject
//
//  Created by Sagbo Augustin AFFOGNON on 17/05/2023.
//

import SwiftUI

struct LecteurView: View {
    var album  : Album?
    var body: some View {
        ZStack{
            
            Grid{
                GridRow(alignment: .center){
                    
                    HStack(){
                        Spacer()
                        Image(album?.image ?? "Image")
                            .resizable()
                            .scaledToFill()
                            .frame(width: 80, height: 70)
                            .clipShape(RoundedRectangle(cornerRadius: 10))
                            .shadow(radius: 5) 
                    }
                        VStack(spacing : 2){
                            Text(album?.auteur ?? "No Author" ).lineLimit(1)
                            Text("29 Avril 2021").lineLimit(1)
                        }
                    
                        HStack(spacing : 30){
                            Image(systemName: "play.fill").resizable()
                                .scaledToFill().frame(width :20,height: 25)
                            Image(systemName: "goforward.30").resizable()
                                .scaledToFill().frame(width :40,height: 25)
                            
                        }.frame(height: 50).padding().padding(.horizontal)
                     
                    
                } .padding(.horizontal)
               
            }.background().padding(.vertical).shadow(color: .gray, radius: 80)
        }
    }
}

struct LecteurView_Previews: PreviewProvider {
    static var previews: some View {
        LecteurView(album: Stub().load())
    }
}
