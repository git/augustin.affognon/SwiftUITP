//
//  EpisodeItem.swift
//  MyFirstProject
//
//  Created by etudiant on 18/05/2023.
//

import SwiftUI

struct EpisodeItem: View {
    var body: some View {
        ZStack{
            VStack(alignment: .leading){
                Text("Mardi").font(.caption)
                Text("ADN poubelle : tout n'est pas à jeter").font(.title).bold()
                Text("durée : 00:56:13 - La science, CQFD - par : Etienne Klein - le néant, la grandre question !").font(.title3).lineLimit(3)
           
                   
                Grid{
                GridRow{
                    Group{
                        Image(systemName: "play.fill")
                            .scaledToFill()
                            .frame(width: 20, height: 10)
                            .padding()
                            .clipShape(Circle())
                            .overlay(
                                Circle().stroke(Color.gray) )
                            .background(Color.gray)
                            .clipShape(Circle())
                            .shadow(radius: 10)
                        Text("6 min")
                    }.foregroundColor(WtaColor.font_icon)
                        
                   
                   Divider()
                   
                        
                        Image(systemName: "ellipsis")
                            .scaledToFill()
                            .frame(width: 20, height: 10)
                            .padding()
                        
                        
                    
                }
            }
                
            }.padding()
        }
    }
}

struct EpisodeItem_Previews: PreviewProvider {
    static var previews: some View {
        EpisodeItem()
    }
}
