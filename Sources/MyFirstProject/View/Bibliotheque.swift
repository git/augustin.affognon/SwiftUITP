//
//  Bibliotheque.swift
//  MyFirstProject
//
//  Created by etudiant on 16/05/2023.
//

import SwiftUI

struct Bibliotheque: View {
    var body: some View {
        NavigationView{
            NavigationStack{
                ScrollView{
                    LazyVGrid(columns: createGridColumns(), spacing: 10) {
                        ForEach(Stub().loadAll(), id: \.nom) { item in
                            NavigationLink{
                                ContentView(album: item,myContenView: CardImage(album: item))
                            } label: {
                                BookCard(album: item)
                            }
                        }
                    }
                }
                .navigationBarHidden(true)
                
                
                
            }.navigationTitle("Podcasts")
            
         /*   .navigationBarItems(
                
                leading:
                    Button(action: {
                     
                    }) {
                        Grid(alignment: .top){
                            GridRow {
                                Image(systemName: "chevron.left")
                                     .scaledToFill()
                                    .frame(width: 20, height: 10)
                                    .padding()
                                    .clipShape(Circle())
                                    .overlay(
                                        Circle().stroke(Color.gray) )
                                    .background(Color.gray)
                                                .clipShape(Circle())
                                                .shadow(radius: 10)
                                 
                               }
                        }
                    },
                trailing:   Button(action: {
                    // Action du bouton de droite
                }) {
                    
                            
                            Image(systemName: "ellipsis")
                                .scaledToFill()
                                .frame(width: 20, height: 10)
                                .padding()
                                .clipShape(Circle())
                                .overlay(
                                    Circle().stroke(Color.gray) )
                                .background(Color.gray)
                                .clipShape(Circle())
                                .shadow(radius: 10)
                            
                        }
                        
                        
                    
                )*/
        }
        
    }
}

struct Bibliotheque_Previews: PreviewProvider {
    static var previews: some View {
        Bibliotheque()
    }
}
private func createGridColumns() -> [GridItem] {
       return Array(repeating: GridItem(.flexible(), spacing: 1), count: 2)
   }
 
