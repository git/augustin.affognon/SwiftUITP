//
//  MyContenView.swift
//  MyFirstProject
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI
import Combine

struct CardImage: View {
    @State public var color: Color = .clear

    var album : Album?
    var body: some View {
       
            
              
        GridRow{
            Rectangle()  .background(.gray)
                       .overlay(
                        Image(album?.image ?? "Image")
                       )
                       .frame(width: 180,height: 180)
                       .clipShape(RoundedRectangle(cornerRadius: 20))
              }
         
        }
        
        
            
         
    }
    
    struct CardImage_Previews: PreviewProvider {
        static var previews: some View {
            CardImage()
        }
    }

 

