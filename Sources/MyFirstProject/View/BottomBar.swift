import SwiftUI
import UIKit
struct BottomBar: View {
    @State private var selectedTab: Tab = .bibliotheque
    
    enum Tab {
        case home
        case bibliotheque
        case favorites
        case settings
    }

    var album  : Album?
    var isDisplaylector : Bool = false
    var body: some View {
        
        TabView(selection: $selectedTab) {
            Group{
                HomeView()
                    .tabItem {
                        Image(systemName: "play.circle.fill")
                        
                        Text("Ecouter")
                    }
                    .tag(Tab.home)
                
                FavoritesView()
                    .tabItem {
                        Image(systemName: "tablecells")
                        Text("Favoris")
                    }.foregroundColor(WtaColor.font_icon)
                    .tag(Tab.favorites)
                
                BibliothequeView()
                    .tabItem {
                        Image(systemName: "list.bullet.below.rectangle")
                        Text("Biblithèque")
                    }
                    .tag(Tab.bibliotheque)
                
                SettingsView()
                    .tabItem {
                        Image(systemName: "shower.sidejet.fill")
                        Text("Recherche")
                    }
                    .tag(Tab.settings)
            }
                
        }
    }
    
    
    struct HomeView: View {
        var body: some View {
            Text("Explorer")
        }
    }
    
    struct FavoritesView: View {
        var body: some View {
            Text("..........")
        }
    }
    struct BibliothequeView: View {
        var body: some View {
            
                Bibliotheque()
             
        }
    }
    struct SettingsView: View {
        var body: some View {
            Text("..........")
        }
        

        
        struct BottomBar_Previews: PreviewProvider {
            static var previews: some View {
                BottomBar(album: Stub().load())
            }
        }
    }
    
}
 
