//
//  Stub.swift
//  MyFirstProject
//
//  Created by etudiant on 09/05/2023.
//

import Foundation
public struct  Stub {
    
    func load()->Album{
        
        return Album(nom: "LE MOINE", type: "France Culture", image: "Moine", ranking: 12)
    }
    
    func loadAll()->[Album]{
        
        return [Album(nom: "LE MOINE", type: "France Culture", image: "Moine", ranking: 12),
                Album(nom: "MONDIO", type: "Monde Culture", image: "Image", ranking: 12),
                Album(nom: "CHA", type: "Monde  Culture ", image: "Image", ranking: 122),
                Album(nom: "Indian d'Amerique du nord", type: "From India ", image: "Indien", ranking: 122)
        ]
    }
    
}
// environnement(.\
